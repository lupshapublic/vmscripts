#!/bin/bash

MYHOSTNAME=`/usr/bin/hostname`
MYMAC=`/usr/sbin/ifconfig | grep -A 5 "eth0" | grep "ether" | head -n 1 | awk '{print $2}'`

# look for me in file, ex: 86:4d:0a:5f:31:8a=k1
LINE=`grep "${MYMAC}" /opt/auto/vmscripts/hosts/mactohostname`

WANTED_HOSTNAME=`echo $LINE | cut -f2 -d=`

if [ "${WANTED_HOSTNAME}" == "" ]; then
  speak "hostname needed in mac file"
elif [ "${WANTED_HOSTNAME}" == "${MYHOSTNAME}" ]; then
  echo "my host name is OK, matches mac file: ${WANTED_HOSTNAME}"
else
  MESSAGE="now changing hostname from $MYHOSTNAME to $WANTED_HOSTNAME"
  echo $MESSAGE
  speak $MESSAGE
  /opt/auto/vmscripts/changehostname.sh "${WANTED_HOSTNAME}"
fi
