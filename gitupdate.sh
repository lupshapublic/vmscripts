#!/bin/bash

cd /opt/auto/vmscripts

git fetch --all
git reset --hard origin/main
git pull

# make all new shell scripts executable
chmod u+x *.sh
