#!/bin/bash
# performs a CURL with a message to the text to speech interface

MESSAGE="$*"
# replace the spaces in the user message with PLUS signs
MESSAGE=${MESSAGE// /+}

# URL of text to speech interface
URL=http://speak.florida:8080/?speak=

# send the message
curl -s "${URL}${MESSAGE}" > /dev/null &
