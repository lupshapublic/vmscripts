#!/bin/bash

while ! ping -c 1 -W 1 10.1.10.1; do
  echo "Waiting for network..."
  sleep 5
done

# say "server SERVERNAME is up and running"
/opt/auto/vmscripts/speak.sh "server `hostname` is up and running"

