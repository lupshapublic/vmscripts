#!/bin/bash

echo "Now adding a link to /opt/scripts/loginnotification.sh to the bottom of your ~./bashrc file"

echo "" >> ~/.bashrc
echo "# BeginLupshaResearchClusterLoginNotification" >> ~/.bashrc
echo "/opt/auto/vmscripts/loginnotification.sh" >> ~/.bashrc
echo "# EndLupshaResearchClusterLoginNotification" >> ~/.bashrc
echo "" >> ~/.bashrc

