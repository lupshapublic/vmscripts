# vmscripts - VMs run a git pull and execute standard tasks at each reboot

Setup:

```
# one time setup, creates /opt/auto and git clones project
curl -sSL https://gitlab.com/lupshapublic/vmscripts/-/raw/main/sudosetup.once | bash
```

Note:
```
# This will be automatically added to the @reboot crontab:
# @reboot /opt/auto/vmscripts/onreboot.sh

# This will be added to the end of the user's ~/.bashrc
# /opt/auto/vmscripts/loginnotification.sh
```

Optional:
```
# install docker in 1 line
curl -sSL https://gitlab.com/lupshapublic/vmscripts/-/raw/main/installdockerdebian.sh | bash
```
