#/bin/bash

INFO="login"
HOSTNAME=`cat /etc/hostname`
USER=$(whoami)
CONNECTEDFROM=$(echo $SSH_CONNECTION | cut -d " " -f 1)
DATE=`date +"%Y-%m-%d"`
TIME=`date +"%H:%M:%S"`
TIMEZONE=`date +"%Z"`

# speak it
IRCMESSAGE="$INFO on $HOSTNAME user ${USER} from $CONNECTEDFROM on $DATE $TIME $TIMEZONE"

# dump to NULL: >/dev/null 2>&1 &
# nohup /opt/settings/scripts/ircnotify.sh "${IRCMESSAGE}" >/dev/null 2>&1 &


SPEAKMESSAGE="login ${HOSTNAME} user ${USER}"
speak "${SPEAKMESSAGE}"
