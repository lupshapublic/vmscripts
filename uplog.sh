#!/bin/bash

# keep a log of server up
NOW=`date "+%Y-%m-%d %H:%M:%S"`
echo "server up at: ${NOW}" >> up.log
