#!/bin/bash

# run this on shutdown
HOSTNAME=`hostname`
/opt/auto/vmscripts/speak.sh "server $HOSTNAME shutting down"
