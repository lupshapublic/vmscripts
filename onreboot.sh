#!/bin/bash

# this should be called from crontab on every reboot, ex:
# crontab -e
# @reboot /opt/auto/vmscripts/onreboot.sh

cd /opt/auto/vmscripts

# do a git update
./gitupdate.sh

# log that we're up
./uplog.sh

# notify that we're up
sleep 10 && ./upnotify.sh
